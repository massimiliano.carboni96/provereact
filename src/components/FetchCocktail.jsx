import React from 'react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Card, Button, Row, } from 'react-bootstrap'
import styled from 'styled-components'
import CartContext from '../context/Cart/CartContext'


export default function FetchCocktail() {
  
  const [fetched, setFetched] = useState([]);
  


  const MyContainer = styled.div`
    display:flex;
    margin-top:5%;
    justify-content:space-evenly;
    

    `



  useEffect(() => {
    axios
      .get('https://www.thecocktaildb.com/api/json/v1/1/search.php?f=m')
      .then(response => {
        console.log(response)
        setFetched(response.data.drinks);

      })



  }, [])


  return (
    <div>

      <Row xs={1} md={4} className="g-4" style={{ margin: '20px', justifyContent: 'center' }}>
        {

          fetched?.map((drinks, index) => {
            return (


              <Card style={{ margin: 5, }}>
                <Card.Img variant="top" src={drinks.strDrinkThumb} />
                <Card.Body>
                  <Card.Title>{drinks.strDrink}</Card.Title>
                  <Card.Text>
                    {drinks.strCategory}
                  </Card.Text>
                  <MyContainer>
                  
                    
                    <Button variant="primary" style={{ borderRadius: '15px' }}>
                      Ingredienti
                    </Button>
                    
                    



                  </MyContainer>
                </Card.Body>

              </Card>


            );


          })


        }
      </Row>



    </div>
  )
}
