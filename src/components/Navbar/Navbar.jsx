import React,{useState} from 'react'


import Burger from './Burger';
import NavContent from './NavContent';


export default function Navbar() {
    const[open,setOpen]=useState(false);

    return(
    
        <>
        <NavContent open={open}/>
        <Burger open={open} onClick={()=>setOpen(! open)}/>
        </>
        
    )
  
}
