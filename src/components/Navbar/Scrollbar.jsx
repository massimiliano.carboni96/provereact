import React from 'react'
import styled from 'styled-components'
import { useWindowScroll } from 'react-use'
import { useEffect } from 'react'
import { useState } from 'react'



export default function Scrollbar() {
    const[scrolled,setScrolled]=useState(0);

    const{x,y}=useWindowScroll();
    
    useEffect(()=>{
        const height= document.documentElement.scrollHeight - document.documentElement.clientHeight;
         setScrolled(((y/height)*100))
    },[y]);
    
    
    const Container=styled.div`
    position:fixed;
    top:0;
    left:0;
    width:100%;
    height:7px;
    z-index:1000;
    `
    const Indicator=styled.div`
    height:100%;
    background:blue;
    width:${scrolled}%
    `

    


    return (
        <Container>
            <Indicator>

            </Indicator>
            
        </Container>
    )
}
