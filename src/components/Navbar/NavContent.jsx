
import Scrollbar from './Scrollbar';
import styled from 'styled-components'
import LoginIcon from '@mui/icons-material/Login';
import { Link } from 'react-router-dom';
import React from 'react'
import ShoppingCartTwoToneIcon from '@mui/icons-material/ShoppingCartTwoTone';





const NavContent = (props) => {

    const NavConteiner = styled.div`
    display:flex;
    justify-content:flex-end;
    border:none;
    background-color:black;
    min-height:5rem; 
    align-items:center;
    color:white;
    font-weight:bold;
    text-decoration:none;
    padding-right:5%;
    @media(max-width: 768px) {
        padding-right:0;
    }
    
    
    
    
    
    `

    const BlackBar=styled.div`
    @media(min-width: 768px) {
        display:none;
    
    }
    width:100vw;
    min-height:5rem;
    z-index:1000;
    background-color:black;
    position:fixed;
    
    `

    const NavRight = styled.div`
    
 
align-content: space-around;
  gap: 4rem;
  display: flex;
  z-index:1;

  @media (max-width: 768px) {
    position: fixed;
    flex-flow: column nowrap;
    right: 0;
    width: 100%;
    height: 100vh;
    padding-top: 20%;
    transform: ${(props) => props.open ? "translateY(50%)" : "translateY(-100%)"};
    transition: width .35s ease-in-out;
    padding-left:5%;
    
    background-color: black;
  }

    
    

    
    `
        

    const Home = styled.div`
    
    
    `
    const Products = styled.div`
    
    `

    const Login = styled.div`
    

    `
    const Cart = styled.div`
    
    `

        


    return (


        <NavConteiner >
            <BlackBar/>

            <NavRight id='Right' open={props.open}>
                <Link to='/' style={{ color: 'inherit', textDecoration: 'inherit' }}>
                    <Home>Home</Home>
                </Link>
                <Link to='/Products' style={{ color: 'inherit', textDecoration: 'inherit' }}>
                    <Products>Products</Products>
                </Link>
                <Link to='/Login' style={{ color: 'inherit', textDecoration: 'inherit' }}>
                    <Login style={{ display: 'flex' }}><LoginIcon />Login</Login>
                </Link>
                <Link to='/Cart' style={{ color: 'inherit', textDecoration: 'inherit' }}><Cart><ShoppingCartTwoToneIcon/></Cart>
                </Link>
            </NavRight>
            <Scrollbar />


        </NavConteiner>




    )
}
export default NavContent;