
import React from 'react'
import styled from 'styled-components'





const StyledBurger = styled.div`

display:block;
position:fixed;
width:2rem;
height:2rem;
right:15px;
top:3vh;
justify-content:space-around;
flex-flow: column nowrap;
margin-right:2%;
z-index:1000; 
div{
    
    width: 2rem;
    height: 0.25rem;
    border-radius:5px;
    background-color: ${(props) => props.open ? '#ccc' : 'white'};
    margin: 6px;
    transform-origin:1px;
    transition: all 0.6s linear;
    

    &:nth-child(1){
        transform:${(props) => props.open ? 'rotate(45deg)' : 'rotate(0deg)'};
    }
    &:nth-child(3){
        transform:${(props) => props.open ? 'rotate(-45deg)' : 'rotate(0deg)'};
    }
    &:nth-child(2){
        transform:${(props) => props.open ? 'translateX(100%)' : 'translateX(0%)'};
        opacity:${(props) => props.open ? 0 : 1};
    }
    
    
}
@media(min-width: 768px) {
        display:none;

`
    ;

export default function Burger(props) {


    return (
        <StyledBurger onClick={() => props.onClick()} open={props.open} >
            <div />
            <div />
            <div />
        </StyledBurger>
    )
}
