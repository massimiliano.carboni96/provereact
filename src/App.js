import './App.css';
import Homepages from './pages/Homepages';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from './pages/Login';
import Products from './pages/Products';
import Cart from './pages/Cart';
import 'bootstrap/dist/css/bootstrap.min.css';



function App() {
  return ( 
      <>
      <Router>
            <Routes>
          
            <Route path='/' element={<Homepages/>}/>
            <Route path='/Login' element={<Login/>}/>
            <Route path='/Products' element={<Products/>}/>
            <Route path='/Cart' element={<Cart/>}/>
            </Routes>
        
      
      
      </Router>
        
        </>
      
      
    
  );
}

export default App;
