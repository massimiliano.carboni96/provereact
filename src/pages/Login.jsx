import React from 'react'
import Navbar from '../components/Navbar/Navbar'
import { Button, Form } from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css';
import styled from 'styled-components'





export default function Login() {

  const FormLogin = styled.div`
display:flex;
justify-content:center;
align-items:center;
margin-top:10vh;

`


  return (
    <div>
      <Navbar />
      <FormLogin>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" />
            <Form.Text className="text-muted">

            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </FormLogin>



    </div>
  )
}
