

import React from 'react'
import FetchCocktail from '../components/FetchCocktail'
import Navbar from '../components/Navbar/Navbar'


export default function Products() {
    return (
        <div>
            <Navbar />
            <FetchCocktail />
        </div>
    )
}
